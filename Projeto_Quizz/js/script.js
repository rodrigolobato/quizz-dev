// Declaração de Variáveis

const question = document.querySelector("#question");
const answersBox = document.querySelector("#answers-box");
const quizzContainer = document.querySelector("#quizz-container");
const scoreContainer = document.querySelector("#score-container");
const letters = ["a", "b", "c", "d"];
let points = 0;
let actualQuestion = 0;


// Puerguntas Quizz 

const questions = [
    {
        "question":"PHP foi desenvolvido para qual finalidade?",
        "answer": [
            {
                "answer":"Back-end",
                "correct": true

            },
            {
                "answer":"Front-end",
                "correct": false

            },
            {
                "answer":"Banco de Dados",
                "correct": false

            },
            {
                "answer":"Moble",
                "correct": false

            }
        ]

    },
    {
        "question":"Uma forma de declarar variavel em JavaScript:",
        "answer": [
            {
                "answer":"$var",
                "correct": false

            },
            {
                "answer":"var =>",
                "correct": false

            },
            {
                "answer":"lete",
                "correct": false

            },
            {
                "answer":"var",
                "correct": true

            }
        ]

    },
    {
        "question":"Um arquivo Json é tratado como?",
        "answer": [
            {
                "answer":"\"chave\" : \"var\"",
                "correct": false

            },
            {
                "answer":"\"chave\" : \"number\"",
                "correct": false

            },
            {
                "answer":"\"chave\" : \"valor\"",
                "correct": true

            },
            {
                "answer":"\"var\" : \"chave\"",
                "correct": false

            }
        ]

    }
    
]

//  Substituir primeira questão do quizz no HTML

function init(){

    // Criar a primeira pergunta
    createQuestion(0);
}


// Criar uma pergunta
function createQuestion(i){

    // Limpar a questão anterior
    const oldButtons = answersBox.querySelectorAll("button");

    oldButtons.forEach(function(btn){
        btn.remove();
    });
  
    // Alterar o texto da pergunta
    const questionText = question.querySelector("#question-text");
    const questionNumber = question.querySelector("#question-number");

    questionText.textContent = questions[i].question;
    questionNumber.textContent = i + 1;

    // Inserindo alternativas
    questions[i].answer.forEach(function(answer, i){

        // Clonando modelo de resposta para substituir alternativas
        const answerTemptate = document.querySelector(".answer-template").cloneNode(true);

        const letterBtn = answerTemptate.querySelector(".btn-letter");
        const answerText = answerTemptate.querySelector(".question-answer");
         
        letterBtn.textContent = letters[i];
        answerText.textContent = answer['answer'];

        answerTemptate.setAttribute("correct-answer", answer["correct"]);

        // Remover hide e template class 
        answerTemptate.classList.remove("hide");
        answerTemptate.classList.remove("answer-template");

        // Adicionar alternativa nas perguntas
        answersBox.appendChild(answerTemptate)

        // Atribuir evento de click nos buttons das alternativas de cada questão
        answerTemptate.addEventListener("click", function(){
            checkAnswer(this);
        });        

    });

    // incrementar número a questão
    actualQuestion++;

}

// Checkando alternativa selecionada
function checkAnswer(btn){

    const buttons = answersBox.querySelectorAll("button");

    // Verificar alternativa correta
    buttons.forEach(function(button){

        if(button.getAttribute("correct-answer") === "true"){
            button.classList.add("correct-answer");


            // Resposta Correta
            if(btn === button){
                points++;
            } 
        }
        else{
            button.classList.add("wrong-answer");
        }

    });

    // Próxima pergunta
    nextQuestion();

}

// Exibir próxima pergunta
function nextQuestion(){

    setTimeout(function(){

        // verificar se o quizz finalizou
        if(actualQuestion >= questions.length){
        showSuccessMessage();
        return;
        
        }
        
        createQuestion(actualQuestion);

    },1000);
}

function showSuccessMessage(){

  hideOrShowQuizz();

    // Calculo do score 
    const score = ((points / questions.length) * 100).toFixed(2);

    const displayScore = document.querySelector("#display-score span");

    displayScore.textContent = score.toString();


    // Marcador perguntas e acertos
    const questionCorrect = document.querySelector("#correct-answers");
    const questionQty = document.querySelector("#questions-qty");

    questionCorrect.textContent = points;
    questionQty.textContent = questions.length;

}

function hideOrShowQuizz(){
    quizzContainer.classList.toggle("hide");
    scoreContainer.classList.toggle("hide");
}


// Reiniciar Quizz 
const restartBtn = document.querySelector("#restart");

restartBtn.addEventListener("click", function(){

    actualQuestion = 0;
    points = 0;
    hideOrShowQuizz();
    init();
});


// Iniciando Quizz

init();